(function ($) {
    Drupal.behaviors.commerce_kickstartslideshow_custom = {
        attach: function(context, settings) {
            if (typeof $.fn.bxSlider != 'undefined') {
                // bx Slider.
                var slider = $('.event-slider', context);
                slider.bxSlider({
                    auto: true,
                    pager: false,
                });
            };
        },
    }
})(jQuery);
