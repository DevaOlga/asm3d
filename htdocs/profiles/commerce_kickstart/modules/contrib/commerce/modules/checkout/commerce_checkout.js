(function($) {

/**
 * Disable the continue buttons in the checkout process once they are clicked
 * and provide a notification to the user.
 */
Drupal.behaviors.commerceCheckout = {
  attach: function (context, settings) {
    // When the buttons to move from page to page in the checkout process are
    // clicked we disable them so they are not accidentally clicked twice.
    $('input.checkout-continue:not(.checkout-processed)', context).addClass('checkout-processed').click(function() {
      var $this = $(this);
      $this.clone().insertAfter(this).attr('disabled', true).next().removeClass('element-invisible');
      $this.hide();
    });
  }
}

    var SmartLocation;

    if (SmartLocation == undefined) {
        SmartLocation = function (settings) {
            this.init(settings);
        };
    }

    SmartLocation.URL_PLACES     = 'https://www.smartpost.ee/places.js';

    SmartLocation.ID_CONTAINER   = 'smartpost_cont';
    SmartLocation.ID_SELECT      = 'smartpost_select';
    SmartLocation.ID_INPUT       = 'smartpost_input';
    SmartLocation.NEXT_ID        = 0;

    SmartLocation.prototype.init = function (settings) {
        this.curId = "" + (SmartLocation.NEXT_ID++);
        this.settings = settings;
        this.selectedId = this.settings.selected_id || 0;
        this.initSettings();

        var container = document.getElementById(this.settings.target_id);
        if(container) {
            container.innerHTML = this.getWidgetHTML();
            this.fetchData(function(){
                this.fillSelect();
                this.select.onchange();
            });
        } else {
            throw new Error("SmartPOST widget container `" + this.settings.target_id + "` not found");
        }

    };

    SmartLocation.prototype.initSettings = function () {
        this.ensureDefault = function (settingName, defaultValue) {
            if(!this.settings) {this.settings = new Array();}
            this.settings[settingName] = (this.settings[settingName] == undefined) ? defaultValue : this.settings[settingName];
        };

        this.ensureDefault("target_id",  "smartpost_widget_target");
        this.ensureDefault("placeid_name", "smartpost_terminal_id");
        this.ensureDefault("placename_name", "smartpost_terminal_name");
        this.ensureDefault("default_id",  "-");


        delete this.ensureDefault;
    };


    SmartLocation.prototype.fetchData = function (callback) {

        if(window['places_info'] == undefined){

            var head = document.getElementsByTagName("head").item(0);

            var id = "smartpostjs";
            var oScript = document.getElementById(id);
            if (oScript) head.removeChild(oScript);

            oScript = document.createElement("SCRIPT");

            oScript.setAttribute("src", SmartLocation.URL_PLACES);
            oScript.setAttribute("type", "text/javascript");
            oScript.setAttribute("id",id);
            head.appendChild(oScript);

        }
        this.onPlacesAvailable.call(this, callback);
    };

    SmartLocation.prototype.onPlacesAvailable = function(callback) {
        if (window['places_info'] != undefined) {
            callback.call(this);
        } else {
            var t = this;
            setTimeout(function () {
                t.onPlacesAvailable.call(t, callback);
            }, 50);
        }
    };

    SmartLocation.prototype.fillSelect = function() {
        this.placesMap = new Array();

        // Dummy entry
        this.placesMap[this.settings.default_id] = {
            "place_id":this.settings.default_id,
            "name":"-",
            "city":"-",
            "address":"-",
            "opened":"-",
            "description":"-"
        };

        this.select = document.getElementById(SmartLocation.ID_SELECT + this.curId);
        var buffer = new Array();

        if(this.settings.show_default){
            var option = document.createElement('option');
            option.value=this.settings.default_id;
            option.appendChild(document.createTextNode(this.settings.text_default_item));
            this.select.appendChild(option);
        }

        for(var i = 0; i < places_info.length; i++) {
            var place      = places_info[i];
            var next_place = null;

            if(i < places_info.length-1) {
                next_place = places_info[i+1];
            }

            if(!next_place || next_place.group_id != place.group_id) {
                var optgroup = document.createElement('optgroup');
                optgroup.label = place.group_name;
                buffer.push(place);

                for(var j = 0; j < buffer.length;j++) {
                    var option = document.createElement('option');
                    option.appendChild(document.createTextNode(buffer[j].name));
                    option.value = buffer[j].place_id;
                    optgroup.appendChild(option);
                    if(this.selectedId == option.value){
                        option.selected = true;
                    }
                }

                buffer = new Array();
                this.select.appendChild(optgroup);
            } else {
                buffer.push(place);
            }

            this.placesMap[place.place_id] = place;
        }
        this.select.onchange = this.setSelected;
        this.select.smartLocation = this;
    };

    SmartLocation.prototype.setSelected = function() {
        var el = document.getElementById('smartpost_select0');
        var text = el.options[el.selectedIndex].innerHTML;
        document.getElementById('smartpost_input0').value = text;
    };

    SmartLocation.prototype.getWidgetHTML = function() {

        return '<div id="'+SmartLocation.ID_CONTAINER + this.curId+'" >'+

            '<div class="smartpost_right">' +
            '<table class="smartpost_table">' +
            '<tr> ' +
            '<td class="smartpost_label">'+this.settings.text_place+'</td>' +
            '<td class="smartpost_input"><select name="'+this.settings.placeid_name+'" id="'+SmartLocation.ID_SELECT + this.curId+'"></select></td>' +
            '</tr></table>' +

            '<input type="hidden" name="'+this.settings.placename_name+'" id="'+SmartLocation.ID_INPUT + this.curId+'" />' +
            '</div>'+
            '<div style="clear: both"></div>'+
            '</div>';
    };


    $(document).ready(function() {
        if ($("#smartpost_widget_target").length) {
            var smartLocation = new SmartLocation(
                {
                    target_id:  "smartpost_widget_target",
                    placeid_name: "smartpost_terminal_id",
                    placename_name: "smartpost_terminal_name",
                    show_default: false,
                    text_place:       "Pakiautomaat",
                    text_default_item: "-- Vali Pakiautomaat --"
                }
            );
        }
    });

})(jQuery);
