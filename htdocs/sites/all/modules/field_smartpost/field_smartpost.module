<?php
/**
 * @file
 *
 * Smartpost field module.
 */

/***************************************************************
 * Field Type API hooks
 ***************************************************************/

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function field_smartpost_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'field_smartpost_place_id' => array(
      'label' => t('Smartpost terminal field'),
      'description' => t('Show us smartpost terminal locations'),
      'default_widget' => 'field_smartpost_select',
      'default_formatter' => 'field_smartpost_place_id',
    ),
  );
}

/**
 * Implements hook_field_validate().
 * Validate the field content.
 * @see field_example_field_widget_error()
 */
function field_smartpost_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['place_id'])) {
      if (!is_numeric($item['place_id'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'field_smartpost_invalid',
          'message' => t('You must select Terminal location.'),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 *
 * hook_field_is_emtpy() is where Drupal asks us if this field is empty.
 * Return TRUE if it does not contain data, FALSE if it does. This lets
 * the form API flag an error when required fields are empty.
 */
function field_smartpost_field_is_empty($item, $field) {
  return empty($item['place_id']);
}

/**
 * Implements hook_field_formatter_info().
 *
 * I could define more that one display.
 * @see field_example_field_formatter_view()
 */
function field_smartpost_field_formatter_info() {
  return array(
    // Create smartpost listing in field.
    'field_smartpost_place_id' => array(
      'label' => t('Smartpost list'),
      'field types' => array('field_smartpost_place_id'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 *
 * @see field_example_field_formatter_info()
 */
function field_smartpost_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'field_smartpost_place_id':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          // See theme_html_tag().
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => field_smartpost_show_place_data($item['place_id']),
        );
      }
      break;
      // I could start next formatter in here..
  }

  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * @see field_smartpost_field_widget_form()
 */
function field_smartpost_field_widget_info() {
  return array(
    'field_smartpost_select' => array(
      'label' => t('Smartpost field select list'),
      'field types' => array('field_smartpost_place_id'),
    ),
    // could create next widget type in here.
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * hook_widget_form() is where Drupal tells us to create form elements for
 * our field's widget.
 *
 */
function field_smartpost_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['place_id']) ? $items[$delta]['place_id'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {
    case 'field_smartpost_select':
    $list = field_smartpost_place_selection();
    $html = field_smartpost_show_place_data($value);
      $widget += array(
        '#type' => 'select',
        '#options' => $list,
        '#default_value' => $value,
        '#ajax' => array(
          'callback' => 'field_smartpost_show_field_data',
          'wrapper' => 'replace_textfield_div',
        ),
        '#suffix' => "<div id=\"replace_textfield_div\">
        $html
        </div>",
      );
  }
  $element['place_id'] = $widget;
  return $element;
}

/**
 * Implements hook_field_widget_error().
 *
 * hook_field_widget_error() lets us figure out what to do with errors
 * we might have generated in hook_field_validate(). Generally, we'll just
 * call form_error().
 *
 * @see field_example_field_validate()
 * @see form_error()
 */
function field_smartpost_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'field_smartpost_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Helper functions.
 * Save the smartpost data into database when cron runs.
 */
function field_smartpost_cron() {
  field_smartpost_rebuild_list();
}

/**
 * Build and rebuild list for smartpost places info.
 */
function field_smartpost_rebuild_list() {
  $uri = field_smartpost_filepath_uri();
  if (!file_exists($uri)) {
    drupal_mkdir($uri);
  }
  if (file_exists($uri)) {
    $smartpost = 'http://smartpost.ee/places.json';
    $smartpost_local = $uri . '/places.json';
    $data = drupal_http_request($smartpost);
    file_unmanaged_save_data($data->data, $smartpost_local, FILE_EXISTS_REPLACE);
  }
}

/**
 * Create smartpost select list (grouped by city).
 */
function field_smartpost_place_selection() {
  $places = field_smartpost_json_to_array();
  $out = array("" => t("Select"));
  foreach ($places as $place) {
    $out[$place['group_name']][$place['place_id']] = $place['name'];
  }
  return $out;
}
/**
 * Create filepath.
 */
function field_smartpost_filepath_uri() {
  return variable_get('file_public_path', conf_path() . '/files/smartpost');
}

/**
 * Create array from post24.json data.
 */
function field_smartpost_json_to_array() {
  $places = file_get_contents(field_smartpost_filepath_uri() . '/places.json');
  return drupal_json_decode($places);
}

/**
 * Create array grouped by place_id.
 */
function field_smartpost_placesbyid() {
  $places = field_smartpost_json_to_array();
  foreach ($places as $place) {
    $output[$place['place_id']] = array(
      'name'    => $place['name'],
      'city'    => $place['city'],
      'address' => $place['address'],
      'opened'  => $place['opened'],
    );
  }
  return $output;
}

function field_smartpost_show_field_data($form, $form_state) {
  $place = field_smartpost_placesbyid();
  $pid = $form_state['triggering_element']['#value'];
  $html = field_smartpost_show_place_data($pid);
  return $html;
}

function field_smartpost_show_place_data($pid) {
  if (!empty($pid)) {
    $place = field_smartpost_placesbyid();
  $t_name = t('Parcel terminal location');
  $t_opened = t('Parcel terminal is accessible at');
  return "<div id=\"replace_textfield_div\">
  <label> $t_name:</label> {$place[$pid]['name']}<br />
          <label> $t_opened:</label> {$place[$pid]['opened']}
          </div>";
  }
  else {
    return '<div id="replace_textfield_div"></div>';
  }
}
