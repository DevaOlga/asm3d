<?php

/**
 * Implements hook_commerce_dpd_settings_info().
 */
function commerce_dpd_commerce_dpd_settings_info() {

  return array(
    'sandbox_live_option' => array(
      'default' => 'sandbox',
    ),
    'api_partner_name' => array(
      'default' => '',
    ),
    'api_partner_token' => array(
      'default' => '',
    ),
    'cloud_user_id' => array(
      'default' => '',
    ),
    'user_token' => array(
      'default' => '',
    ),
    'debug_messages' => array(
      'default' => 0,
    ),
    'debug_log' => array(
      'default' => FALSE,
    ),
    'label_size' => array(
      'default' => 'PDF_A4',
    ),
    'label_position' => array(
      'default' => 'UpperLeft',
    ),
    'salutation' => array(
      'default' => '',
    ),
    'weight' => array(
      'default' => '',
    ),
    'content' => array(
      'default' => '',
    ),
    'reference1' => array(
      'default' => '',
    ),
    'reference2' => array(
      'default' => '',
    ),
  );
}
